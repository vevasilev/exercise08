import java.util.*;

public class CustomHashMap<K, V> {

    static final int DEFAULT_INITIAL_CAPACITY = 16;
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private Node<K, V>[] hashTable;
    private int size;
    private float threshold;

    public CustomHashMap() {
        hashTable = new Node[DEFAULT_INITIAL_CAPACITY];
        threshold = hashTable.length * DEFAULT_LOAD_FACTOR;
        size = 0;
    }

    public Set<K> keySet() {
        Set<K> keySet = new HashSet<>();
        for (Node<K, V> node : hashTable) {
            while (node != null) {
                keySet.add(node.getKey());
                node = node.getNext();
            }
        }
        return keySet;
    }

    public Collection<V> values() {
        Collection<V> values = new ArrayList<>();
        for (Node<K, V> node : hashTable) {
            while (node != null) {
                values.add(node.getValue());
                node = node.getNext();
            }
        }
        return values;
    }

    public V put(final K key, final V value) {
        if (size + 1 > threshold) {
            threshold *= 2;
            resize();
        }
        int hash = getHashCode(key);
        int index = (hashTable.length - 1) & hash;
        Node<K, V> newNode = new Node<>(hash, key, value);
        Node<K, V> elementTable = hashTable[index];
        if (elementTable == null) {
            hashTable[index] = newNode;
        } else {
            Node<K, V> previousElement = null;
            while (elementTable != null) {
                if (elementTable.getHash() == hash && (Objects.equals(key, elementTable.getKey()))) {
                    V oldValue = elementTable.getValue();
                    elementTable.setValue(value);
                    return oldValue;
                }
                previousElement = elementTable;
                elementTable = elementTable.getNext();
            }
            previousElement.setNext(newNode);
        }
        size++;
        return null;
    }

    public V get(final K key) {
        int hash = getHashCode(key);
        int index = (hashTable.length - 1) & hash;
        if (index > hashTable.length) {
            throw new ArrayIndexOutOfBoundsException("There is no element with such a key");
        } else if (hashTable[index] != null) {
            for (Node<K, V> node : hashTable) {
                while (node != null) {
                    if (key.equals(node.getKey())) {
                        return node.getValue();
                    }
                    node = node.getNext();
                }
            }
        }
        return null;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public V remove(final K key) {
        int hash = getHashCode(key);
        int index = (hashTable.length - 1) & hash;
        if (index > hashTable.length) {
            throw new ArrayIndexOutOfBoundsException("There is no element with such a key");
        } else if (hashTable[index] == null) {
            return null;
        } else if (hashTable[index].getNext() == null && key.equals(hashTable[index].getKey())) {
            V valueRemoveNode = hashTable[index].getValue();
            hashTable[index] = null;
            size--;
            return valueRemoveNode;
        } else {
            Node<K, V> previousNode = hashTable[index];
            Node<K, V> removeNode = hashTable[index];
            while (removeNode != null) {
                if (key.equals(removeNode.getKey()) && previousNode == removeNode) {
                    V valueRemoveNode = removeNode.getValue();
                    hashTable[index] = removeNode.getNext();
                    size--;
                    return valueRemoveNode;
                } else if (key.equals(removeNode.getKey()) && previousNode != removeNode) {
                    V valueRemoveNode = removeNode.getValue();
                    previousNode.setNext(removeNode.getNext());
                    size--;
                    return valueRemoveNode;
                }
                previousNode = removeNode;
                removeNode = removeNode.getNext();
            }
        }
        return null;
    }

    public boolean containsKey(final K key) {
        int hash = getHashCode(key);
        int index = (hashTable.length - 1) & hash;
        if (index > hashTable.length) {
            throw new ArrayIndexOutOfBoundsException("There is no element with such a key");
        } else if (hashTable[index] != null) {
            for (Node<K, V> node : hashTable) {
                while (node != null) {
                    if (key.equals(node.getKey())) {
                        return true;
                    }
                    node = node.getNext();
                }
            }
        }
        return false;
    }

    public boolean containsValue(final V value) {
        for (Node<K, V> node : hashTable) {
            while (node != null) {
                if (value.equals(node.getValue())) {
                    return true;
                }
                node = node.getNext();
            }
        }
        return false;
    }

    public int size() {
        return size;
    }

    public void clear() {
        if(size != 0) {
            for (Node<K, V> node : hashTable) {
                while (node != null) {
                    remove(node.getKey());
                    node = node.getNext();
                }
            }
        }
    }

    private void resize() {
        Node<K, V>[] oldHashTable = hashTable;
        hashTable = new Node[oldHashTable.length * 2];
        size = 0;
        for (Node<K, V> node : oldHashTable) {
            while (node != null) {
                put(node.getKey(), node.getValue());
                node = node.getNext();
            }
        }
    }

    private int getHashCode(K key) {
        if (key != null) {
            int hash = 31;
            hash = hash * 17 + key.hashCode();
            return hash;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "CustomHashMap{" + Arrays.toString(hashTable) + '}';
    }
}