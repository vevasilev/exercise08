public class Exercise08 {
    public static void main(String[] args) {
        CustomHashMap<Integer, String> map = new CustomHashMap<>();
        System.out.println("Adding new elements to custom HashMap:");
        map.put(1, "a");
        map.put(2, "b");
        map.put(3, "c");
        map.put(4, "d");
        System.out.println(map);
        System.out.println();

        System.out.println("Getting the value of elements by key:");
        System.out.println(map.get(1));
        System.out.println(map.get(2));
        System.out.println(map.get(3));
        System.out.println();

        System.out.println("Let's check if custom HashMap contains elements with the given keys \"2\" and \"5\": ");
        System.out.println("Key \"2\": " + map.containsKey(2));
        System.out.println("Key \"5\": " + map.containsKey(5));
        System.out.println();

        System.out.println("Let's check whether custom HashMap contains elements with the specified values: ");
        System.out.println("Value \"a\": " + map.containsValue("a"));
        System.out.println("Value \"h\": " + map.containsValue("h"));
        System.out.println();

        System.out.println("Getting a list of all keys and values: ");
        System.out.println("Keys: " + map.keySet());
        System.out.println("Values: " + map.values());
        System.out.println();

        System.out.println("Remove the value from custom HashMap by the key \"1\" and \"5\": ");
        System.out.println("The value of the element to be deleted with the key \"1\": " + map.remove(1));
        System.out.println("The value of the element to be deleted with the key \"5\": " + map.remove(5));
        System.out.println("Keys: " + map.keySet());
        System.out.println("Values: " + map.values());
        System.out.println();

        System.out.println("Remove all elements from custom HashMap");
        map.clear();
        System.out.println("Custom HashMap is empty? " + map.isEmpty());
        System.out.println(map);
    }
}